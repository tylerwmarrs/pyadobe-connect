import requests
from xml.dom.minidom import parseString
from pyadobe_connect.utils import Utils

class BadHttpResponse(Exception):
    pass

class TextToXml(Exception):
    pass

class NoAccess(Exception):
    pass

class NoData(Exception):
    pass

class Invalid(Exception):
    pass

class TooMuchData(Exception):
    pass

class MissingUrl(Exception):
    pass

class InvalidData(object):
    """This is still in the works, but the plan is that when a Adobe Connect
    returns an Invalid response you will receive a list of InvalidData objects.
    """
    def __init__(self, field, type, subcode):
        self.field = field
        self.type = type
        self.subcode = subcode

    def __str__(self):
        return "Field - %s, Type - %s, SubCode - %s" % (
            self.field,
            self.type,
            self.subcode
        )

def singleton(cls):
    """Transforms a class into a singleton."""
    instances = {}
    def getinstance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class Connector(Utils):
    """This class handles all of requests to the Adobe Connect API and it
    transforms the XML response into a DOM object. This class is a singleton class so
    it can maintain cookie state which helps reduce the number of requests and
    keeps you authenticated."""
    _instance = None
    DEBUG = False

    # status codes
    OK = 'ok'
    INVALID = 'invalid'
    NOACCESS = 'no-access'
    NODATA = 'no-data'
    TOOMUCHDATA = 'too-much-data'

    def __init__(self, debug=False):
        """Set up debug (defaults to False) and set cookies to None."""
        self.DEBUG = debug
        self.cookies = None
        self.url = None

    def set_url(self, url):
        """Sets the url to connect to."""
        self.url = url

    def clear_cookies(self):
        """Clears all of the current cookie values."""
        self.cookies = None

    def _parse_response(self, response):
        """Here we check the dom for particular status codes that 
        Adobe provides and raise exceptions accordingly.

        invalid (Invalid) - Indicates that the call is invalid in some way, usually invalid syntax.

        no-access (NoAccess) - Shows that the current user does not have permission to
        call the action, and includes a subcode attribute with more information.

        no-data (NoData) - Indicates that there is no data available for the action to
        return, when the action would ordinarily return data.

        too-much-data (TooMuchData) - Means that the action should have returned a single
        result but is actually returning multiple results.When the status code
        is invalid, the response also has an invalid element that shows which
        request parameter is incorrect or missing
        """
        try:
            dom = parseString(response)
        except:
            raise TextToXml, 'Unable to transform the text to xml.'
        # parse for the status code
        status = dom.getElementsByTagName('status')[0]
        status_value = status.attributes['code'].value

        # got the ok return the dom
        if status_value == self.OK:
            return dom

        if status_value == self.NOACCESS:
            # there should be a sub code
            subcode_value = status.attributes['subcode'].value
            raise NoAccess, 'Received no-access code. Subcode: %s' % subcode_value

        if status_value == self.INVALID:
            # parse all of the information for the invalid tags
            invalid_nodes = status.getElementsByTagName('invalid')

            invalids = []

            for node in invalid_nodes:
                field = node.getAttributeNode('field').nodeValue
                field_type = node.getAttributeNode('type').nodeValue
                subcode = node.getAttributeNode('subcode').nodeValue

                invalid = InvalidData(field, field_type, subcode)
                invalids.append(invalid)
                if self.DEBUG:
                    print invalid

            self.invalids = invalids
            raise Invalid

        if status_value == self.NODATA:
            raise NoData

        if status_value == self.TOOMUCHDATA:
            raise TooMuchData, 'A single result should be returned, but multiple results were found.'

    def get(self, params):
        """Makes a get request to the url provided and returns an XML dom
        object back. 
        """
        
        if not self.url:
            raise MissingUrl, 'URL is required to make requests.'

        r = requests.get(self.url, params=params, cookies=self.cookies)

        if self.DEBUG:
            print "URL:", r.url
            print "HEADERS:", r.headers

        # if there is not a cookie set lets set one and use it from now on
        if not self.cookies:
            self.cookies = r.cookies

        if self.DEBUG:
            print "RESPONSE:", r.text

        if r.ok:
            return self._parse_response(r.text)

        else:
            raise BadHttpResponse, 'Did not receive HTTP 200 response.'
