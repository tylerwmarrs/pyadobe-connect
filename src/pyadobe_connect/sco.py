from pyadobe_connect.utils import Utils
from pyadobe_connect.connector import Connector

class Sco(Utils):
    """An Sco is a content representation in Adobe Connect. It could be a
    meeting archive, folder, image, file, etc...
    """
    id = None
    host = None
    source = None
    folder = None
    type = None
    icon = None
    display_seq = None
    is_folder = None
    name = None
    uri = None
    begin = None
    end = None
    modified = None
    duration = None

    # bind attributes to sco attributes
    ATTRIBUTE_BINDINGS = (
        ('id', 'sco-id'),
        ('source', 'source-sco-id'),
        ('folder', 'folder-id'),
        ('type', 'type'),
        ('icon', 'icon'),
        ('display_seq', 'display-seq'),
        ('is_folder', 'is-folder'),
        ('name', 'name'),
        ('uri', 'url-path'),
        ('begin', 'date-begin'),
        ('end', 'date-end'),
        ('modified', 'date-modified'),
        ('duration', 'duration'),
    )

    def __init__(self, *args, **kwargs):
        """Instantiates the Connector object so we can make requests from this
        object.
        """
        self.__dict__.update(kwargs)
        self.connector = Connector()

    @property
    def url(self):
        return "%s%s" % (self.host, self.uri)

    def __str__(self):
        return "(%s)%s - %s" % (self.icon, self.name, self.id)
