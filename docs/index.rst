.. Python Adobe Connect documentation master file, created by
   sphinx-quickstart on Wed Apr  3 13:13:00 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python Adobe Connect's documentation
================================================

This is a library that interfaces with the Adobe Connect API. The goal is to make working with the API much easier by allowing you to work with native python types. It emphasizes OOP concepts to make life easy.

Contents:

.. toctree::
   :maxdepth: 2

   modules
   pyadobe_connect

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

