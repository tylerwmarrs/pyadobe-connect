from pyadobe_connect.adobeconnect import AdobeConnect
from pyadobe_connect.connector import Connector
from pyadobe_connect.meeting import Meeting

from nose.tools import *

class TestParseMeetings(object):

    @classmethod
    def setup_class(self):
        self.adobe = AdobeConnect()
   
    @classmethod
    def teardown_class(self):
        self.adobe = None

    def test_parse_meetings(self):
        xml = """<?xml version="1.0" encoding="utf-8"?>
        <results><status code="ok"/><report-bulk-objects><row
        sco-id="945424050" type="meeting"
        icon="meeting"><url>/defaulteventtemplate/</url><name>Default Event Template</name>
        <date-modified>2010-06-02T12:01:54.957-07:00</date-modified></row><row
        sco-id="945424049" type="meeting"
        icon="meeting"><url>/defaulttrainingtemplate/</url><name>Default Training Template</name>
        <date-modified>2010-06-02T12:01:54.957-07:00</date-modified></row><row
        sco-id="945424048" type="meeting"
        icon="meeting"><url>/defaultmeetingtemplate/</url><name>Default Meeting
        Template</name><date-modified>2010-06-02T12:01:54.957-07:00</date-modified>
        <date-created>2010-06-02T12:01:54.957-07:00</date-created>
        <date-end>2010-06-02T12:01:54.957-07:00</date-end>
        </row>
        </report-bulk-objects></results>"""

        connector = Connector()
        dom = connector._parse_response(xml)
        meetings = self.adobe._parse_meetings(dom)

        assert_is_instance(meetings, list)
        assert_equals(len(meetings), 3)

        assert_is_instance(meetings[0], Meeting)
        assert_is_instance(meetings[1], Meeting)

        meeting = meetings[0]

        assert_equals(meeting.id, '945424050')
        assert_equals(meeting.uri, '/defaulteventtemplate/')
        assert_equals(meeting.name, 'Default Event Template')
        assert_is_none(meeting.created)
        assert_is_none(meeting.end)
        assert_equals(meeting.modified, '2010-06-02T12:01:54.957-07:00')


        meeting = meetings[2]
        assert_equals(meeting.created, '2010-06-02T12:01:54.957-07:00')
        assert_equals(meeting.end, '2010-06-02T12:01:54.957-07:00')
