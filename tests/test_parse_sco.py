from pyadobe_connect.adobeconnect import AdobeConnect
from pyadobe_connect.connector import Connector
from pyadobe_connect.sco import Sco 

from nose.tools import *

class TestParseSco(object):

    @classmethod
    def setup_class(self):
        self.adobe = AdobeConnect()
   
    @classmethod
    def teardown_class(self):
        self.adobe = None

    def test_parse_sco(self):
        xml = """<?xml version="1.0" encoding="utf-8"?><results><status
        code="ok"/><scos><sco sco-id="1184644719" source-sco-id="1184606450"
        folder-id="1168134978" type="content" icon="producer" display-seq="0"
        duration=""
        is-folder="0"><name>/p1925ezn82t/</name><url-path>/p1925ezn82t/</url-path><date-created>2013-02-12T16:27:36.493-08:00</date-created><date-modified>2013-02-12T16:27:36.493-08:00</date-modified></sco></scos></results>"""

        connector = Connector()
        dom = connector._parse_response(xml)
        scos = self.adobe._parse_scos(dom)

        assert_is_instance(scos, list)
        assert_equals(len(scos), 1)

        assert_is_instance(scos[0], Sco)

        sco = scos[0]

        assert_equals(sco.id, '1184644719')
        assert_equals(sco.source, '1184606450')
        assert_equals(sco.folder, '1168134978')
        assert_equals(sco.type, 'content')
        assert_equals(sco.icon, 'producer')
        assert_equals(sco.display_seq, False)
        assert_equals(sco.duration, None) 
        assert_equals(sco.is_folder, False)
        assert_equals(sco.name, '/p1925ezn82t/')
        assert_equals(sco.uri, '/p1925ezn82t/')
        assert_equals(sco.modified, '2013-02-12T16:27:36.493-08:00')
