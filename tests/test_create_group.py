from pyadobe_connect.adobeconnect import AdobeConnect
from pyadobe_connect.connector import Connector, Invalid
from pyadobe_connect.principal import Principal

from nose.tools import *

class TestCreateGroup(object):

    @classmethod
    def setup_class(self):
        self.adobe = AdobeConnect()
   
    @classmethod
    def teardown_class(self):
        self.adobe = None

    def test_success_message(self):
        xml = """<?xml version="1.0" encoding="utf-8"?>
        <results><status code="ok"/><principal type="group" has-children="true"
        account-id="945424023" principal-id="1220372769"><description>This is a test group</description>
        <name>Testing111</name><login>Testing111</login></principal></results>"""
        connector = Connector()
        dom = connector._parse_response(xml)
        principal = self.adobe._parse_new_group(dom)

        assert_is_instance(principal, Principal)

        assert_equals(principal.id, '1220372769')
        assert_equals(principal.account, '945424023')
        assert_equals(principal.type, 'group')
        assert_equals(principal.children, True)
        assert_equals(principal.description, 'This is a test group')
        assert_equals(principal.name, 'Testing111')
        assert_equals(principal.login, 'Testing111')

    @raises(Invalid)
    def test_invalid_missing_name(self):
        xml = """<?xml version="1.0" encoding="utf-8"?><results><status
        code="invalid"><invalid field="name" type="string"
        subcode="missing"/></status></results>"""

        connector = Connector()
        dom = connector._parse_response(xml)
        principal = self.adobe._parse_new_group(dom)
        assert_is_none(principal)

    @raises(Invalid)
    def test_invalid_duplicate(self):
        xml = """<?xml version="1.0" encoding="utf-8"?><results><status
        code="invalid"><invalid field="login" type="string"
        subcode="duplicate"/></status></results>"""

        connector = Connector()
        dom = connector._parse_response(xml)
        principal = self.adobe._parse_new_group(dom)
        assert_is_none(principal)
