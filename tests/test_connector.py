from pyadobe_connect.connector import (
    Connector,
    NoAccess,
    NoData,
    Invalid,
    TooMuchData,
    TextToXml
)

from nose.tools import *

class TestConnectorResponses(object):

    @classmethod
    def setup_class(self):
        self.connector = Connector()
   
    @classmethod
    def teardown_class(self):
        self.connector = None

    @raises(NoAccess)
    def test_Exception_NoAccess(self):
        xml = """<?xml version="1.0" encoding="utf-8" ?><results><status
        code="no-access" subcode="denied" /></results>"""
        self.connector._parse_response(xml)

    @raises(NoData)
    def test_Exception_NoData(self):
        xml = """<?xml version="1.0" encoding="utf-8"?><results><status
        code="no-data"/></results>"""
        self.connector._parse_response(xml)

    @raises(Invalid)
    def test_Exception_Invalid(self):
        xml = """<?xml version="1.0" encoding="utf-8" ?><results><status code="invalid"><invalid field="has-children" type="long" subcode="missing" /></status></results>"""
        self.connector._parse_response(xml)

    @raises(TooMuchData)
    def test_Exception_TooMuchData(self):
        xml = """<?xml version="1.0" encoding="utf-8" ?><results><status
        code="too-much-data"></status></results>"""
        self.connector._parse_response(xml)

    @raises(TextToXml)
    def test_Exception_TextToXml(self):
        xml = """<?xml parsing fail ?><results></results>"""
        self.connector._parse_response(xml)
